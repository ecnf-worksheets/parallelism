﻿using System;
using System.Diagnostics;
using System.IO;

namespace Parallelism
{
    class Program
    {
        static void Main()
        {
            int[] rangeValues = {20, 200, 2000, 20000, 200000, 2000000};
            double[] times = new double[rangeValues.Length];
            Func<int, int[]> calculatePrime;

            // just activate the calculation methode of your choice
            calculatePrime = CalculatePrimesDemo.CalculatePrimesSequential;
            //calculatePrime = CalculatePrimesDemo.CalculatePrimesThreads;
            //calculatePrime = CalculatePrimesDemo.CalculatePrimesParallelFor;
            // calculatePrime = CalculatePrimesDemo.CalculatePrimesLinq;
            // calculatePrime = CalculatePrimesDemo.CalculatePrimesPLinq;

            Console.WriteLine($"Calculate Primes for {rangeValues.Length} Ranges");
            int i = 0;
            for (i = 0; i < rangeValues.Length; i++)
            {
                var stopWatch = new Stopwatch();
                stopWatch.Start();

                var primes = calculatePrime(rangeValues[i]);
                times[i] = stopWatch.ElapsedMilliseconds;
                Console.WriteLine($"{i+1}. Range [3..{rangeValues[i],8:D0}]: {times[i]}ms");
            //                PrintPrimes(primes);
            }

            // This methods prints the data directly into a csv file, appends the data 
            // the various runs into one file.
            // Find the file in the runtime directory of the app
            //PrintTimesToCsvFile(calculatePrime.Method.Name, rangeValues, times);

            Console.WriteLine("Done.");
            Console.ReadKey();
        }

        public static void PrintPrimes(int[] primes)
        {
            foreach (var i in primes)
                Console.WriteLine(i);
        }

        /// <summary>
        /// Prints the time measurements into a CSV file with the ranges as a header
        /// like
        ///                ,[3..      20],[3..     200],[3..    2000],[3..   20000],[3..  200000],[3.. 2000000]
        /// CalculatePrimesSequential,12,0,0,9,273,7197
        /// </summary>
        /// <param name="type">The calculation type as a string (sequential, LIQ, ...) </param>
        /// <param name="rangeValues">the ranges calculated</param>
        /// <param name="times"> the measured results</param>
        public static void PrintTimesToCsvFile(String type, int[] rangeValues, double[] times)
        {
            
            using (var writer = new StreamWriter("timelog.csv", true))
            {
                // print header only once, only if file is not empty
                WriteHeader(rangeValues, writer);

                WriteMeasuremens(type, times, writer);

            }
        }

        private static void WriteMeasuremens(string type, double[] times, StreamWriter writer)
        {
            writer.Write($"{type},");
            for (int i = 0; i < times.Length; i++)
            {
                writer.Write($"{times[i]}");
                if (i < times.Length - 1)
                    writer.Write($",");
                else writer.WriteLine();
            }
        }

        private static void WriteHeader(int[] rangeValues, StreamWriter writer)
        {
            // writer Header only if file is new
            FileInfo fi1 = new FileInfo(((FileStream)(writer.BaseStream)).Name);
            if (fi1.Length > 0) return;

            writer.Write($"               ,");
            for (int i = 0; i < rangeValues.Length; i++)
            {
                writer.Write($"[3..{rangeValues[i],8:D0}]");

                if (i < rangeValues.Length - 1)
                    writer.Write($",");
                else writer.WriteLine(); ;
            }
        }
    }
}
