﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Parallelism
{
    /// <summary>
    /// Implements the various prime calculation methods according to the 
    /// worksheet tasks.
    /// </summary>
    public class CalculatePrimesDemo
    {
        /// <summary>
        /// Calculate the prime numbers from 3 up to the given range. 
        /// </summary>
        /// <param name="range">the upper number of the range</param>
        /// <returns>the found prime numbers in an integer array</returns>
        public static int[] CalculatePrimesSequential(int range)
        {
            var results = new ConcurrentBag<int>();
            for (var number = 3; number < range; number++)
            {
                var foundPrime = true;
                for (var divisor = 2; divisor * divisor <= number; divisor++)
                    if (number % divisor == 0)
                        foundPrime = false;

                if (foundPrime)
                    results.Add(number);
            }

            return results.ToArray();
        }

    }
}
